# My Dygma Defy Layers

## Foreword

I am using non standard keyboard layouts: mainly the French Optimot layout. So these layers can serve as an inspiration for your own layers but you probably won’t be able to reuse them “as is”, unless you’re French and willing to switch to Optimot.

Besides, these layers have already undergone several versions since I have received the Defy, and even if they seem to have stabilized quite a bit now, they are still to be considered as a “work in progress”. In this regard, you’ll find that there is some redundancy: some characters are accessible through different combinations (typically, the numbers can be reached in 3 different places), that’s because I’m still experimenting and will keep only the places that I use most and discard those I never use. It’s all about *usage*.

## Layer 1 - main alpha layer

This layer is the most complex one, and the main layer to type text.

[![Layer 1](pics/L01.jpg)](pics/L01.jpg)

### Main Characters

As outlined in the foreword, this is the Optimot layout, with a slight change since the Defy misses some keys on the right-hand side:

- the French ç, which is moved to both the top row and the thumb cluster (on the key that also Shift to Layer 2),
- the Z character (which is normally to the right of 'X') is moved down for easy access, as it is quite used in English.
- the two most right keys from the top row are moved to the “Symbols” layer instead since those keys don’t exist on the Defy,
- the top left key (backtick and ~ in qwerty) is also moved to another layer, to get the 'Escape' key there,
- the top row stays as symbols rather than letters as in the standard Optimot layout. All those symbols are also easily reachable from the “Symbols” layer.

Note that the center part of the keyboard is “characters only”, no row mods, superkeys or macros. That’s because I’m experimenting with chording, and any “non-simple” key cannot be chorded.

### Modifiers and Special Keys

A quick note about the Defy’s thumb cluster: all keys are easy to reach for me, so I supercharged the thumb clusters. Besides, the two most inward keys (those that require the thumb to reach under the palm) are also very easy to reach with the index finger, which makes it easy for instance for the thumb to be on the Ctrl/Enter key while the index presses '/' or '-' on the thumb cluster.~

Modifiers are now under the thumbs rather than the pinkies to avoid too many stretches. Everything that can be used during text/code typing is under very easy places for the thumbs: Ctrl, Shift, AltGr (which provides very useful layers in Optimot with tons of special unicode characters such as math, arrows and bullets, monetary symbols, characters for many languages, and even 𝔤𝔬𝔱𝔥𝔦𝔠 𝔰𝔠𝔯𝔦𝔭𝔱 or chess pieces), space, backspace and delete, and enter.

For shortcuts that use a lot of combined modifiers such as 'Ctrl-Alt-Shift-L', there are some macros under the right thumb that provide combinations of modifiers. These are placed a bit further since it’s okay to take a little more time for a shortcut than normal typing.

### Layer Shifts

I use mostly layer shifts (press and hold the key which switches to another layer, use that layer, and switch back to layer 1 as soon as the switch key is released), but some layers also have a “lock” access if it makes sense (typically, it makes sense for the navigation or numpad layers, but not much for the “special characters” layer, those are typically for one-shot use). Some layers are also only “lockable”, since they are for an entirely different usage and context: keyboard layout translation layers so that I can still type in Optimot on machines where Optimot is not installed.

### Common Special Characters for Thumbs

I have moved some special characters that I use quite often to the thumb cluster, they are generally also available elsewhere, and this is the result of “maximum exposure” of characters during testing those layouts to keep only the most used places on the long term.

- 'dash' character '-' is in a very easy place to reach near the space character, indeed it’s useful for words that use a dash as separators rather than space. Think about easy-to-reach keys. 🙂 I find it very practical: the thumb simply needs to move slightly to the right to separate words with dashes instead of spaces.
- the '*' character is a very common character both in programming and command-line usage, so it has its own very reachable position on the left thumb,
- similarly, the '/' is extremely common (think file and directory paths…), besides it is just near the '.' above, which makes things like '../.' quite satisfying to type,
- on the left hand, the '#' is also in an easy place, it is used typically for comments in many places for a programmer (bash, python…),
- on the right hand, many macros which I’m not sure will stay with time: easy access for characters commonly used again for the average computer person: ';:&|\'.
- the German umlaut and ß are also under an easy-to-reach position - they are otherwise available in Optimot, but a little tedious to type.

### Common Shortcuts

I have made many Superkeys for very frequently used combinations:

- F5 and Ctrl-F5, F11 and Ctrl-F11 as well as Ctrl-Space and Ctrl-1 (for the Eclipse IDE), Ctrl-F4 (close current tab) and Alt-F4 (close current window).
- a Copy/Paste superkey that has served me very well so far on the far bottom left of the keyboard  (the Shift versions are mostly used in terminals, where Ctrl-C is already taken to abort a process):
    - tap⇒Ctrl-C,
    - Hold⇒Ctrl-V,
    - double tap⇒Ctrl-Shift-C,
    - double tap hold⇒Ctrl-Shift-V,
    - tap hold⇒Ctrl-X.
- backspace combinations like Ctrl-Backspace or Alt-Backspace.

## Layer 2 - numpad and function pad

This layer is dedicated to a full numpad on the right hand, and a function pad on the left hand. It also has media controls.

[![Layer 2](pics/L02.jpg)](pics/L02.jpg)

I originally had the numpad on the left hand, but I couldn’t get rid that easily of 40 years of conditioning with the numpad under the right hand. I’m sure I could have with time, but in this specific case, I preferred to move it to the right hand, and put a “function pad” on the left hand. Indeed, while it’s quite common to type long numbers or mathematical operations with the numpad (think “123+456”), it’s rare to do that with the function keys!

### Extended Numpad

The numpad is a very classical configuration, extended to the thumb cluster for the bottom part. As mentioned earlier, the most inner thumb keys are easily reached with the index and middle finger.

I’ve also added the parentheses, useful for simple operations, as well as backspace and del. On the right, the '^' sign is also used for exponents.

### Function Keys

Although we are used to function keys arranged in a row, I find that it is much easier to reach them in the form of a numpad. The main advantage is that it can be done with a single hand and without the need to move the hand around at all. I typically use function keys for debugging, so navigating from F5 (step into in Eclipse) to F7 (exit function) is seamless and done with the same hand in an intuitive manner.

I have also added all modifier combinations on the thumb cluster, so that it is easy to get them from a single hand with modifiers: Alt-F4, Ctrl-F5, etc.

### Other special keys

A mix of other keys that I couldn’t put anywhere else ended up on that layer: media keys, laptop screen brightness, battery level, pause and scroll lock.

## Layer 3 - Navigation and Emojis

[![Layer 3](pics/L03.jpg)](pics/L03.jpg)

### Navigation Side

Along with the normal arrows and home/end keys, I have added some other tools that are used while navigating around:

- mouse wheel (scrolling) movements,
- the obvious page up/down,
- tab, del, backspace, space,
- all modifier combinations on the pinky, in order to easily navigate or select in text or in workspaces on Linux (including moving windows around): Ctrl, Shift, Alt. I find this way of navigating around very satisfying and intuitive to use. It’s especially nice to simply move the pinky one row down to switch from “word-to-word” navigation to “word-to-word” selection.

I didn’t know where to put the Print Screen keys, so they ended up in that layer.

### Emoji Side

I like to use a lot of emojis in text since it is very difficult while chatting to know the emotions of the person on the other side of the line. I naturally placed the most common ones on the home row, and tried to find places that are easy to remember for others.

Note that on the thumb cluster of that side sit the full modifier combinations as well, so that if I don’t feel like using the pinky on the right hand, I can also use them there. So far, I feel very comfortable on the right hand using the pinky, but I still give a chance to that home cluster, maybe I’ll get used to it with time and it will overtake the pinky usage, I simply need to give it a try and find out what is most comfortable.

## Layer 4 - Symbols and Numbers

[![Layer 4](pics/L04.jpg)](pics/L04.jpg)

This layer is dedicated to symbols that are not so reachable on the first (main) layer. Some keys are also supercharged for commonly-used combinations, such as ' - ' and ' + ' (space-separated subtraction and addition characters).

I have also added the numbers for easier access, since I find that my hand has trouble reaching the top row of the Defy. I still placed the function keys on the top row, but my guess is that I will never use them at all, since they are easily reached on the Functions layer.

A few macros are also present on this layer for very special symbols like the Ğ which is the symbol for a libre currency that I sometimes use.

## Layers 5 and 6 - abbreviated syllables

This is definitely “work in progress”. The idea being that some syllables are very common, and can be accessed directly rather than typing them. Is it faster? Probably not. But overall, it's less keys to type, less mistakes to make and fix, and for long “syllables” or in fact common n-grams like “ation” in English, it’s even sometimes faster. The difference between those two layers is that one in more focused on English and the other on French. I am currently limited by a bug in the firmware of the Defy that doesn’t allow more than 61 macros, but I do hope to fill those layers a little bit more in the future (I already have statistics on most used n-grams in English and French).

[![Layer 5](pics/L05.jpg)](pics/L05.jpg)
[![Layer 6](pics/L06.jpg)](pics/L06.jpg)

## Layer 7 - Gimp Shortcuts

A new layer that will contain shortcuts, essentially for gimp, but potentially for other programs too. However, it is yet to be completed because I have reached the maximum number of macros on Bazecor, so I’ll have to wait for the Dygma team to fix that before filling it further.

[![Layer 7](pics/L07.jpg)](pics/L07.jpg)

## Layer 8 - Mouse Actions

[![Layer 8](pics/L08.jpg)](pics/L08.jpg)

I think everything is in the title. The only thing that maybe needs commenting is the extra keys around the main central keys. Those are attached to macros that perform a number of mouse moves in a row to simulate bigger mouse steps. These are buggy for now but will be fixed by Dygma at some point so I’ll probably use that layer a lot more than today.

## Layer 9/10 - Dvorak/Azerty ⇒ Optimot

These two layers are useful when connecting to machines that can’t easily benefit from the Optimot layout. Typically, most of my Linux machines start up in Dvorak by default as it is the main layout I have been using for the last 20 years. And as I live in France, I am bound to have contacts with machines configured in Azerty. These layers simply reconfigure the main keys so that I can still use the Optimot layout even when connected on machines where it is not installed. Incidentally, those two layers are totally identical when using them on the given layout, except for the left thumb cluster, where the Alt key has to be in a different position because of a limitation in Bazecor.

I have also optimized the special characters to fit in reachable places, while also keeping them in easy places to remember for me (typically by mirroring my “symbols” layer when possible).

[![Layer 9](pics/L09.jpg)](pics/L09.jpg)
[![Layer 10](pics/L10.jpg)](pics/L10.jpg)

# History

## 2024/01/22

- general: slightly changed some colors to make them more different and identifiable,
- layer 2: added '.' and ',' on the numpad layer on the thumb cluster as they are useful when typing numbers in different locales,
- layer 3: changed two emoticons and moved clown,
- layer 3: added system key as it can be combined with other keys,
- layer 8: added alt, shift and play/pause on the mouse layer as I start using this layer a little more (still missing mouse movements in macros in Bazecor, though).

## 2023/12/28

- added mouse backward/forward in the center,
- replaced & by _ on the right thumb cluster,
- added a kdenlive super key on the numpad layer,
- added tab on the numpad layer,
- added gimp shortcut layer, incomplete for now because of current the limitation on the number of macros in Bazecor,
- added underscore on the right thumb for the conversion layers (Dvorak and Azerty).

## 2023/12/09

- big color changes, especially on L1
- on L2, added non breaking space and the thousands separator on the pinky with a superkey,
- on L3, added the [Del] key on the thumb cluster, directly accessible with the index finger,
- a few more words on the English n-grams layer,
- on the mouse layer (L8), inverted the Mouse Forward and Menu keys, so that the Menu key is found under the Mouse Right Button, which makes more sense,







